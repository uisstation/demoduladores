Este repositorio contiene toda la información requerida para la instalación de modulos OTT en GNU Radio, herramientas que fueron implementadas en el proyecto de la estación terrena UIS.

1. Instalación del módulo [gr-satngos](https://gitlab.com/librespacefoundation/satnogs/gr-satnogs)

2. Instalación del módulo [gr-satellites](https://github.com/daniestevez/gr-satellites)

3. Osmosdr, ofrece librerias necesarias para instalar los módulos para receptores de radio según el dispositivo que se desea implementar, consutar librería de dongle en [osmocom](https://osmocom.org/projects/gr-osmosdr/wiki)

